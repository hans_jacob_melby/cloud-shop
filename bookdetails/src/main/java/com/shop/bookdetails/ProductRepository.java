package com.shop.bookdetails;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Collection;

/**
 * Created by hjm on 02.12.2016.
 */
public interface ProductRepository extends MongoRepository<Book,Long>{
    @RestResource(path = "by-title")
    Collection<Book> findByTitle(@Param("title") String title);
}
