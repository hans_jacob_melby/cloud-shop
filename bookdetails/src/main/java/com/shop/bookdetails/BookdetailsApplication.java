package com.shop.bookdetails;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;

@SpringBootApplication
public class BookdetailsApplication {

	@Bean
	CommandLineRunner runner(ProductRepository pr) {
		return args -> {

			pr.deleteAll();

			ArrayList<Book> inventory = new ArrayList<>();
			inventory.add(new Book("Lord of The rings","Fantasy","Tolkien"));
			inventory.add(new Book("The Holy Bible","Religion","GOD"));
			inventory.add(new Book("Lets Make america Great Again","horror","Trumph"));
			inventory.add(new Book("Me me, and only me","Fact","Trumph"));
			inventory.add(new Book("Emil","Children","Anne Cat Westli"));


			inventory.forEach(x -> pr.save(x));

			pr.findAll().forEach(System.out::println);
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(BookdetailsApplication.class, args);
	}
}
