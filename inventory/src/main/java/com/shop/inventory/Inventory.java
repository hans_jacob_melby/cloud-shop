package com.shop.inventory;

import javax.persistence.Entity;

import javax.persistence.Id;

/**
 * Created by hjm on 02.12.2016.
 */
@Entity
public class Inventory {
    @Id
    private String productId;
    private int Instock;
    private int onOrder;


    public Inventory(){
    }

    public Inventory(String productId, int instock, int onOrder) {
        this.productId = productId;
        Instock = instock;
        this.onOrder = onOrder;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getInstock() {
        return Instock;
    }

    public void setInstock(int instock) {
        Instock = instock;
    }

    public int getOnOrder() {
        return onOrder;
    }

    public void setOnOrder(int onOrder) {
        this.onOrder = onOrder;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "productId='" + productId + '\'' +
                ", Instock=" + Instock +
                ", onOrder=" + onOrder +
                '}';
    }
}
