package com.shop.inventory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
public class InventoryApplication {

	@Bean
	CommandLineRunner runner(InventoryRepository ir) {
		return args -> {

			ir.deleteAll();

			ArrayList<Inventory> inventory = new ArrayList<>();
			inventory.add(new Inventory("5841cba6ca5bf22438aabfb6",0,0));
			inventory.add(new Inventory("5841cba6ca5bf22438aabfb7",0,0));
			inventory.add(new Inventory("5841cba6ca5bf22438aabfb8",0,0));
			inventory.add(new Inventory("5841cba6ca5bf22438aabfb9",0,0));
			inventory.add(new Inventory("5841cba6ca5bf22438aabfba",0,0));




			inventory.forEach(x -> ir.save(x));

			ir.findAll().forEach(System.out::println);
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(InventoryApplication.class, args);
	}
}
