package com.shop.inventory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Collection;

/**
 * Created by hjm on 02.12.2016.
 */
@RepositoryRestResource
public interface InventoryRepository extends JpaRepository<Inventory, String> {

    @RestResource(path = "by-title")
    Collection<Inventory> findByProductId(@Param("productId") String productId);
}
