package com.shop.order.jpa;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by hjm on 05.12.2016.
 */
@Entity
public class OrderLine {
    @Id
    @GeneratedValue
    private Long id;
    private String productId;
    private int amount;
    private Status status;

    public OrderLine(){
    }
    public OrderLine(String productId, int amount) {

        this.productId = productId;
        this.amount = amount;
        this.status=Status.INIT;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    public Long getId(){
        return id;
    }

    public Status getStatus(){
        return status;
    }

}
     enum Status{
        INIT,ACCEPTED,BILLED,SHIPPED,RECEIVED;
    }