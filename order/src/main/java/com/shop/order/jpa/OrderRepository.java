package com.shop.order.jpa;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Collection;

/**
 * Created by hjm on 05.12.2016.
 */
public interface OrderRepository extends JpaRepository<OrderLine,Long> {

    @RestResource(path = "by-ID")
    Collection<OrderLine> findByid (@Param("id") Long id);
}
