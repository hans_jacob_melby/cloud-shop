package com.shop.order.controllers;

/**
 * Created by hjm on 06.12.2016.
 */
public class OrderWeb {
    private String id;
    private int number;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
