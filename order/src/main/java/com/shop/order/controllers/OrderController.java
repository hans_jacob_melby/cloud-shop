package com.shop.order.controllers;


import com.shop.order.jpa.OrderLine;
import com.shop.order.jpa.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


/**
 * Created by hjm on 05.12.2016.
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    OrderRepository repo;
    @Autowired
    JmsTemplate jmsTemplate;

    @RequestMapping(method = RequestMethod.GET)
    public OrderLine order(){
        OrderLine order = new OrderLine("1234",10);
        OrderLine orderLine = repo.save(order);
        jmsTemplate.setPubSubDomain(true);
        jmsTemplate.convertAndSend("shop.orderAccepted", order);
        return orderLine;
    }
    @RequestMapping(value = " /order/{orderId}", method=RequestMethod.GET)
    public ResponseEntity<OrderLine> getOrder(@PathVariable Long orderId){

         List<OrderLine> orderLine = (List<OrderLine>) repo.findByid(orderId);
        if (orderLine!=null) {
            ResponseEntity<OrderLine> response = new ResponseEntity<OrderLine>(orderLine.get(0),HttpStatus.OK);
            return response;
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.POST)
        ResponseEntity<?> add(@RequestBody final OrderWeb input) {
        OrderLine ol = new OrderLine();
        ol.setAmount(input.getNumber());
        ol.setProductId(input.getId());
        OrderLine result = repo.save(ol);
        URI location = ServletUriComponentsBuilder
            .fromCurrentRequest().path("/{id}")
        .  buildAndExpand(result.getId()).toUri();
        jmsTemplate.setPubSubDomain(false);
        jmsTemplate.convertAndSend("shop.sendOrder", ol);
        return ResponseEntity.created(location).build();
        }
    }

